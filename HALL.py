import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

#Les 4 bits de poids faible correspondent au résultat des capteurs (Exemple de valeur : 0000zyxw) avec w,w,y et z les valeurs prises par les capteurs
#Lorsqu'un capteur détecte un aimant, sont état est à 1, 0 sinon.
active = 0

#w
HALL_1 = 40
#x
HALL_2 = 38
#y
HALL_3 = 37
#z
HALL_4 = 36

GPIO.setup(HALL_1, GPIO.IN)
GPIO.setup(HALL_2, GPIO.IN)
GPIO.setup(HALL_3, GPIO.IN)
GPIO.setup(HALL_4, GPIO.IN)

while True:
    if(GPIO.input(HALL_1) == False):
        active = (active | 1)
    else:
        active = (active & 254)
		
    if(GPIO.input(HALL_2) == False):
        active = (active | 2)
    else:
        active = bin(active & 253)
		
    if(GPIO.input(HALL_3) == False):
        active = (active | 4)
    else:
        active3 = (active & 251)
		
    if(GPIO.input(HALL_4) == False):
        active = (active | 8)
    else:
        active = (active & 247)
    
    print(active)

    time.sleep(.300)
